#include "Window.h"
#include <assert.h>
#include "Messenger/Message.h"
#include "Messenger/Messenger.h"
#include "WindowImplementation.h"
#include "Listener Delegator/ListenerDelegator.h"
#include "Messenger/TypedMessage.h"

#include "Dynamic Array/DynamicArray.inl"
#include "Listener Delegator/ListenerDelegator.inl"
#include "Message Receiver/MessageReceiver.inl"

CWindow* CWindow::ms_pDefaultWindow( NULL );

void CWindow::SetDefaultWindow( CWindow* pWindow )
{ 
	ms_pDefaultWindow = pWindow; 
}

CWindow::CWindow( const i8* kpWindowName, i32 iX, i32 iY, f32 fWidth, f32 fHeight, 
	bool bFullscreen )
	: m_pWindowImplementation( NULL )
{
	assert( fWidth >= 0.f && fHeight >= 0.f );
	m_pWindowImplementation = NWindow::ImplementWindow( kpWindowName, iX, iY, fWidth,
		fHeight, bFullscreen, *this );
}

CWindow::~CWindow()
{
	m_oMessageReceiver.StopListening( *this, CMessenger::GetGlobalMessenger(), 
		FRAME_OPEN_MESSAGE, &CWindow::Push );
	if( m_pWindowImplementation )
	{
		m_pWindowImplementation->Terminate();
		delete m_pWindowImplementation;
	}
}

void CWindow::Draw() const
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->Draw();
}

void CWindow::Draw2D() const
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->Draw2D();
}

f32 CWindow::GetWidth() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetWidth();
}

f32 CWindow::GetHeight() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetHeight();
}

f32 CWindow::GetVirtualWidth() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetVirtualWidth();
}

f32 CWindow::GetVirtualHeight() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetVirtualHeight();
}

f32 CWindow::GetScale() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetScale();
}

void CWindow::Resize( f32 fNewWidth, f32 fNewHeight )
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->Resize( fNewWidth, fNewHeight );
	TTypedMessage< CWindow* > oResizeMessage( ms_kuWindowResizeMessageUID, this );
	CMessenger::GlobalPush( oResizeMessage );
}

bool CWindow::IsOpen() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->IsOpen();
}

CWindowImplementation& CWindow::GetImplementation() const
{ 
	assert( m_pWindowImplementation );
	return *m_pWindowImplementation; 
}

void CWindow::Push( CMessage& rMessage )
{
	assert( rMessage.GetType() == FRAME_OPEN_MESSAGE );
	if( IsOpen() )
	{
		Draw();
	}
}

const i8* CWindow::GetName() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetName();
}

u32 CWindow::GetDrawMessage() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetDrawMessage();
}

u32 CWindow::GetDraw2DMessage() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetDraw2DMessage();
}

u64 CWindow::GetWindowUID() const
{
	assert( m_pWindowImplementation );
	return m_pWindowImplementation->GetWindowUID();
}

void CWindow::Activate()
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->Activate();
}

void CWindow::Show()
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->Show();
	m_oMessageReceiver.Listen( *this, CMessenger::GetGlobalMessenger(), 
		FRAME_OPEN_MESSAGE, &CWindow::Push );
}

void CWindow::SetDirty()
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->SetDirty();
}

void CWindow::ScaleBy( f32 fScale )
{
	assert( m_pWindowImplementation );
	m_pWindowImplementation->ScaleBy( fScale );
}

CWindow::CWindow()
{
}
